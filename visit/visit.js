
    // teste develop
    
    const visit = function visit(node, methods) {
        node.metadados.forEach(metadado =>{
            if (methods.hasOwnProperty('visitCampos')) {
                metadado.campos.forEach(campo =>{
                    methods.visitCampos(campo.atributo)
                })
            }
        })
        
        methods.visitCampos(node.metadados[0])
        return undefined;
    };

    module.exports.visit = visit;