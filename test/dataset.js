var obj =
{
    metadados: [

        {
            id: 'metadado1',
            campos: [
                {
                    atributo: 'campo 11',
                },
                {
                    atributo: 'campo 12',
                }
            ]
        }
        ,
        {
            id: 'metadado2',
            campos: [
                {
                    atributo: 'campo 21',
                },
                {
                    atributo: 'campo 22',
                }
            ]
        }

    ]
}

module.exports.obj = obj;