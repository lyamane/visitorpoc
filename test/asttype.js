var types= require("ast-types");
var b = types.builders;
var visit = types.visit;
var expect = require('expect.js');
var recast = require('recast');

describe('Teste asttype', function() {
    
    it('teste', function() {
        let codigo = 'this.teste.metodo()';
        
        let codigoAst = recast.parse(codigo);

        visit(codigoAst,
        {
            visitMemberExpression(path) {
                console.log(path)
                console.log(recast.print(path.node))
                this.traverse(path);
            }

        });

        expect(codigoAst).to.be.ok();
    });

});